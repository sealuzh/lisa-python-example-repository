import math
from example.ConcreteClass import ConcreteClass
from example.ConcreteSubclass import ConcreteSubclass
from example.subpackage.SubpackageClass import SubpackageClass
from example.subpackage.subsubpackage.SubsubpackageClass import SubsubpackageClass
from examplesibling.SiblingClass import SiblingClass
from example.ControlFlow import ControlFlow
from example.codesmells.BrainMethod import BrainMethod
from example.Demo import Demo
#import org.ifi.seal.example.subpackage.*
#import org.ifi.seal.example.subpackage.subsubpackage.*
#import org.ifi.seal.example_sibling.SiblingClass
#import org.ifi.seal.example.codesmells.*

class Main(object):
    def main(self):

        instance = ConcreteClass()
        instanceConstructedWithParams =  ConcreteClass(1)
        instanceSibling =  SiblingClass()
        instanceSubpackage =  SubpackageClass()
        instanceSubsubpackage =  SubsubpackageClass()
        instanceInnerClass = instance. InnerClass()
        instanceInnerInnerClass = instanceInnerClass. InnerInnerClass()
        nullVar = None

        cf =  ControlFlow()

        brainMethod =  BrainMethod()

        instance.invokesMethodMethod()
        try:
            instance.throwsExceptionMethod(1)
        except: pass

        one = math.cos(math.pi * 2)
        two = 2


        x = 10
        while (x > 0):
          x -= 1
          x -= 1
        assert(x <= 0)

        assertionThing = False
        if (1 > 2): pass
        else:
            if (200 > 100):
                if 2000 > 1000: assertionThing = True

        assert(assertionThing == True)

        assert(instance.returnsSomethingMethod() == 1)
        instance =  ConcreteSubclass()
        assert(instance.returnsSomethingMethod() == 101)

        print("Main function finished")

        d = Demo(40)
        d.run()

Main().main()

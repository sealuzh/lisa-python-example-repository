class Demo(object):
  def __init__(self, x = 0):
    self.x = x
  def run(self):
    for i in range(1, self.x):
      if (i % 3 == 0 and i % 5 == 0):
        print(i)

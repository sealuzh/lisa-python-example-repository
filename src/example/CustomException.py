class CustomException(Exception):
    def __init__(self, message, errors):
        super(CustomException, self).__init__(message)
        self.errors = errors

# UniquePaths: 354
class ControlFlow(object):

    # Members0 - UniquePaths: 2, MCC: 2
    def singleIf(self):
        if (True): pass

    # Members1 - UniquePaths: 3, MCC: 3
    def twoNestedIfs(self):
        if (True):
            if (True): pass

    # Members2 - UniquePaths: 4, MCC: 3
    def twoConsecutiveIfs(self):
        if (True): pass
        if (True): pass

    # Members3 - UniquePaths: 10, MCC: 5
    def simpleIfTree(self):
        # UniquePaths: 5
        if (True):
            if (True):
                if (True): pass
        # UniquePaths: 2
        if (True): pass

    # Members4 - UniquePaths: 24, MCC: 7
    def complexIfTree(self):
        # UniquePaths: 3
        if (True):
            if (True): pass
        # UniquePaths: 2
        if (True): pass
        # UniquePaths: 4
        if (True):
            if (True):
                if (True): pass

    # Members5 - UniquePaths: 2, MCC: 2
    def orOperator(self):
        return False or True

    # Members6 - UniquePaths: 3, MCC: 3
    def orOperatorInsideIf(self):
        if (False or True): pass

    # Members7 - UniquePaths: 4, MCC: 4
    def twoOrOperatorsInsideIf(self):
        if (False or False or True): pass

    # Members8 - UniquePaths: 2, MCC: 2
    def ternaryOperator(self):
        return False if False else True

    # Members9 - UniquePaths: 3, MCC: 3
    def ternaryOperatorInsideIf(self):
        if (False if False else True): pass

    # Members10 - UniquePaths: 2, MCC: 2
    def singleFor(self):
        for i in range (1, 10): pass

    # Members11 - UniquePaths: 3, MCC: 3
    def twoNestedFors(self):
        for i in range (1, 10):
            for j in range (1, 10): pass

    # Members12 - UniquePaths: 4, MCC: 3
    def twoConsecutiveFors(self):
        for i in range(1,10): pass
        for i in range(1,10): pass

    # Members13 - UniquePaths: 2, MCC: 2
    def singleForWithContinue(self):
        for i in range(1,10): continue

    # Members14 - UniquePaths: 2, MCC: 2
    def singleWhile(self):
        b = False
        while (b): pass

    # Members15 - UniquePaths: 3, MCC: 3
    def twoNestedWhiles(self):
        b = False
        while (b):
            while (b): pass

    # Members16 - UniquePaths: 4, MCC: 3
    def consecutiveWhiles(self):
        b = False
        while (b): pass
        while (b): pass

    # Members17 - UniquePaths: 2, MCC: 2
    def singleWhileWithBreak(self):
        b = False
        while (b): break

    # Members18 - UniquePaths: 2, MCC: 2
    def singleWhileWithContinue(self):
        b = False
        while (b): continue

    # Members20 - UniquePaths: 56, MCC: 9
    def complexMixedExample(self):
        b = False
        # UniquePaths: 4
        if (True):
            if (True):
                while (b): pass
        # UniquePaths: 2
        if (True): pass
        # UniquePaths: 12
        if (True):
            # UniquePaths: 3
            if (True):
                for i in range(1,10): pass
            # UniquePaths: 2
            if (True): pass

    # Members21 - UniquePaths: 1, MCC: 1
    def listComprehension(self):
        [x for x in range(1.10)]

    # Members22 - UniquePaths: 1, MCC: 1
    def dictComprehension(self):
        {x for x in range(1.10)}



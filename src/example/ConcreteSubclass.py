from example.ConcreteClass import ConcreteClass

class ConcreteSubclass(ConcreteClass):
    def returnsSomethingMethod(self): return 101

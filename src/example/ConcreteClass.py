from example.CustomException import CustomException

class ConcreteClass:
    instanceVariable = None

    privateField = 1
    protectedField = 1
    publicField = 1

    privateStaticField = 1
    protectedStaticField = 1
    publicStaticField = 1

    def __init__(self, constructorParam = 1):
        instanceVariable = constructorParam


    def mccMethod(self):
        if True:
            if True:
                if True: pass

    def accessesFieldsMethod(self):
        privateField += 1
        protectedField += 1
        publicField += 1

    def hasParametersMethod(self, s, c, i, d, f, l, sh, b): pass

    def returnsSomethingMethod(self): return 1

    def invokesMethodMethod(self):
        self.returnsSomethingMethod()

    def throwsExceptionMethod(self, param):
        raise CustomException("msg", "err")

    class InnerClass(object):

        def f1(self): return 1

        class InnerInnerClass:
            def f2(self): return 2
            def f3(self):
                if True: return 3
                else: return 4

    def addedMethod1(self): pass

    def lambdaExpression(self):
        lambda x: x**2

